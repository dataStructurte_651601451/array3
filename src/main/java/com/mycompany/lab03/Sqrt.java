/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab03;

/**
 *
 * @author pasinee
 */
public class Sqrt {
    public static int mySqrt(int x) {
        if (x == 0 || x == 1) {
            return x;
        }

        long start = 0;
        long end = x;

        while (start <= end) {
            long mid = start + (end - start) / 2;
            long square = mid * mid;

            if (square == x) {
                return (int) mid;
            } else if (square < x) {
                start = mid + 1;
            } else {
                end = mid - 1;
            }
        }

        return (int) end;
    }

    public static void main(String[] args) {
        int x1 = 4;
        System.out.println("Square root of " + x1 + " is: " + mySqrt(x1));

        int x2 = 8;
        System.out.println("Square root of " + x2 + " is: " + mySqrt(x2));
    }
}
